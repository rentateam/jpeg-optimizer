<?php
namespace Astartsky\JpegOptimizer;

use Symfony\Component\Process\Process;

class JpegOptimizer
{
    const BIN = "jpegoptim";

    protected $command;
    protected $isStripEXIF = false;

    /**
     * @param bool $state
     */
    public function setStripEXIF($state)
    {
        $this->isStripEXIF = (bool) $state;
    }

    /**
     * @param string $file
     * @return string
     */
    protected function createCommand($file)
    {
        $options = array(
            self::BIN
        );

        if ($this->isStripEXIF) {
            $options[] = "--strip-all";
        }

        $options[] = $file;

        return implode(" ", $options);
    }

    /**
     * @param string $file
     */
    public function run($file)
    {
        $command = $this->createCommand($file);
        $process = new Process($command);
        $process->run();
    }
}